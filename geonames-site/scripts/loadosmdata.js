xmlDoc    = loadXMLDoc("scripts/map.xml"); //defined in loadxmldoc.js
var x     = xmlDoc.getElementsByTagName("node"),
arrPoints = [];

//prepare data

for (i=0;i<x.length;i++) {	
	var lat = parseFloat(x.item(i).getAttribute('lat')),
	lng     = parseFloat(x.item(i).getAttribute('lon')),
	id      = x.item(i).getAttribute('id')
	arrPoints[i] = {id: id, lat: lat, lng: lng};
}

var marker  = { lat: 21.328735, lng: 39.95234 },
nearest     = arrPoints[0],
cacheDist   = 99999999999, //dirty
nearestNode = 0,
R           = 6371; // km

for (var i = 0; i < arrPoints.length; i++) {
	var d = crowDist(arrPoints[i], marker);
	if (d < cacheDist) { 
		nearestNode = i; 
		cacheDist   = d; 
	}
};

console.log(arrPoints[nearestNode]);
var arrWayNodes = []
function parseWaysXml(xml)
{
    var xml = $(xml);
    xml.find('nd[ref='+arrPoints[nearestNode].id+']').first().parent().children('nd').each(function() {
        //console.log( $(this).attr("ref") );
        var ref = $(this).attr("ref"),
        node    = xml.find('node[id='+ref+']'),
        //google.maps.LatLng(lat,lng);
        lat     = node.attr("lat"),
        lon     = node.attr("lon"),
        latlng  = {lat: lat, lng: lng};
        arrWayNodes.push(latlng);
        //console.log( xml.find('node[id='+ref+']').attr("lat") );
        //console.log( xml.find('node[id='+ref+']').attr("lon") );
    });
    console.log(arrWayNodes);
}

function loadWays()
{
    $.ajax({
        type: "GET",
        url: 'scripts/map.xml',
        dataType: "xml",
        success:parseWaysXml
    });
}

function crowDist(firstCoords, secondCoords) {
	var x = (secondCoords.lng - firstCoords.lng) * Math.cos((firstCoords.lat + secondCoords.lat)/2),
	y = (secondCoords.lat - firstCoords.lat);
	return Math.sqrt(x*x + y*y) * R;
}