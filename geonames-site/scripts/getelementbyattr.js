//Filters the given array to those which when passed into matcher return true
Array.prototype.where = function(matcher) {
    var result = [];
    for (var i = 0; i < this.length; i++) {
        if (matcher(this[i])) {
            result.push(this[i]);
        }
    }
    return result;
};

function GetElementsByAttribute(tag, attr, attrValue, doc ) {
    doc = doc || document ;

    //Get elements and convert to array
    var elems = Array.prototype.slice.call(document.getElementsByTagName(tag), 0);
    
    //Matches an element by its attribute and attribute value
    var matcher = function(el) {
        return el.getAttribute(attr) == attrValue;
    };

    return elems.where(matcher);
}
